function User(name, age){
  this.name = name
  this.age = age
}
User.prototype.compare = function(user){
  if(this.age > user.age){
    return `${ this.name } is elder than ${ user.name }`;
  }
  else{
    return `${ user.name } is elder than ${ this.name }`;
  }
};
user1 = new User("sachin", 21);
user2 = new User("vinay", 20);
document.getElementById("result").innerHTML = user1.compare(user2);
