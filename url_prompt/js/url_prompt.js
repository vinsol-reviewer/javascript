function UrlPrompt(options) {
  this.url;
  this.options = options;
  this.URL_REGEX = new RegExp(/^(https|http):\/\/\w+\..+/);
}

UrlPrompt.prototype.isUrlEmpty = function() {
  return !(this.url);
};

UrlPrompt.prototype.isUrlValid = function() {
  return this.URL_REGEX.test(this.url);
};

UrlPrompt.prototype.getUrl = function() {
  while(true) {
    this.url = prompt("Enter url");
    if (!(this.isUrlValid()) || this.isUrlEmpty()) {
      var error = this.isUrlEmpty() ? "Empty" : "Invalid" ;
      this.showAlert(error);
    }
    else {
      window.open(this.url, "", this.options);
      break;
    }
  }
};

UrlPrompt.prototype.showAlert = function(error) {
  alert(`This url is ${ error }`);
};

window.onload = function() {
  var options = "scrollbars=no, titlebar=no, status=no, width=400px, height=450px";
  var urlPrompt = new UrlPrompt(options);
  urlPrompt.getUrl();
}
