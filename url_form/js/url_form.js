function UrlForm(ids) {
  this.url = ids.url;
  this.submitButton = ids.submitButton;
  this.URL_REGEX = new RegExp(/^(?:https|http):\/\/(\w+\.)?(\w+)(?:\.\w{2,3})(?:\/|\/.+)?/i);
}
UrlForm.prototype.isUrlValid = function() {
  return this.URL_REGEX.test(this.url.value);
};

UrlForm.prototype.getUrlElements = function() {
  return this.URL_REGEX.exec(this.url.value);
};

UrlForm.prototype.showAlert = function(subDomain, domain) {
  if (subDomain) {
    alert(`Domain : ${ domain },Sub-Domain : ${ subDomain }`);
  }
  else {
    alert(`Domain : ${ domain }`);
  }
};

UrlForm.prototype.submitUrl = function() {
  if (this.isUrlValid()) {
    var result = this.getUrlElements();
    this.showAlert(result[1], result[2]);
  }
  else {
    alert("This url is Invalid");
  }
  event.preventDefault();
};

UrlForm.prototype.bindEvent = function() {
  var handler = this;
  this.submitButton.addEventListener('click', function() {
    handler.submitUrl();
  });
};

window.onload = function() {
  var ids = {
    url : document.getElementById("url"),
    submitButton : document.getElementById("submit-form"),
  }
  var newForm = new UrlForm(ids);
  newForm.bindEvent();
};
