function TextInput(ids) {
  this.element = ids.element;
  this.jsonData = ids.jsonData;
}

TextInput.prototype.getSuggesions = function() {
  var str = this.element.value;
  var currentSuggessions = [];
  var regexpString = "^" + str;
  var nameRegex = new RegExp(regexpString);
  for(var i = 0;i < this.jsonData.length; i++) {
    if (str.length && nameRegex.test(this.jsonData[i].name)) {
      currentSuggessions.push(this.jsonData[i].name);
    }
  }
  return currentSuggessions;
};

TextInput.prototype.showSuggesions = function() {
  var div = document.getElementById("suggesions");
  div.innerHTML = "";
  var suggesions = this.getSuggesions();
  for (var i = 0; i < suggesions.length; i++) {
    var pTag = document.createElement("p");
    pTag.innerHTML = suggesions[i];
    div.appendChild(pTag);
  }
};

TextInput.prototype.bindEvent = function() {
  this.element.addEventListener('input', this.showSuggesions.bind(this));
};

window.onload = function() {
  var ids = {
    element: document.getElementById("input"),
    jsonData: [ {"name":"Luigi Damiano"},
                {"name":"Zenith Coboro"},
                {"name":"Zig Ziglar"},
                {"name":"Steve Costner"},
                {"name":"Bill Grazer"},
                {"name":"Timothy Frazer"},
                {"name":"Boris Becker"},
                {"name":"Glenn Gladwich"},
                {"name":"Jim Jackson"},
                {"name":"Aaron Kabin"},
                {"name":"Roy Goldwin"},
                {"name":"Jason Goldberg"},
                {"name":"Tim Ferris"},
                {"name":"Buck Singham"},
                {"name":"Malcom Gladwell"},
                {"name":"Joy Rabura"},
                {"name":"Vid Luther"},
                {"name":"Tom Glicken"},
                {"name":"Ray Baxter"},
                {"name":"Ari Kama"},
                {"name":"Kenichi Suzuki"},
                {"name":"Rick Olson"}]
  };
  var textInput = new TextInput(ids);
  textInput.bindEvent();
};
