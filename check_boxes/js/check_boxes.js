function CheckBoxes(parents) {
  this.parents = parents;
};

CheckBoxes.prototype.relate = function() {
  for (var i = 0; i < this.parents.length; i++) {
    children = document.getElementsByClassName(this.parents[i].getAttribute("value"));
    this.parents[i].childCheckboxes = children;
  }
};

CheckBoxes.prototype.setChildCheckboxVisibility = function(parentCheck) {
  var check = parentCheck.checked;
  for (var i = 0; i < parentCheck.childCheckboxes.length; i++) {
    parentCheck.childCheckboxes[i].checked = check;
  }
  var view = document.getElementById(parentCheck.getAttribute("value"));
  if(check) {
    view.classList.remove("hidden");
    var parentDiv = parentCheck.parentNode;
    parentDiv.scrollIntoView();
  }
  else {
    view.classList.add("hidden");
  }
};

CheckBoxes.prototype.onChange = function(parentCheck) {
  this.setChildCheckboxVisibility(parentCheck);
};

CheckBoxes.prototype.setEventListners = function() {
  for (i = 0; i < this.parents.length; i++) {
    this.parents[i].addEventListener('change', this.onChange.bind(this, this.parents[i]));
  }
};

window.onload = function() {
  var parents = document.getElementsByClassName("parent");
  var checkboxes = new CheckBoxes(parents);
  checkboxes.relate();
  checkboxes.setEventListners();
};
