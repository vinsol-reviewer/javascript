function MoveElements(ids) {
  this.add = ids.add;
  this.remove = ids.remove;
  this.sourceList = ids.sourceList;
  this.destinationList = ids.destinationList;
}

MoveElements.prototype.eventListener = function(element) {
  if (element == this.add) {
    this.move(this.sourceList, this.destinationList);
  }
  else {
    this.move(this.destinationList, this.sourceList);
  }
};

MoveElements.prototype.move = function(sourceList, destinationList) {
  var selectedList = sourceList.selectedOptions;
  var len = selectedList.length;
  for(i = 0; i < len; i++) {
    destinationList.appendChild(selectedList[0]);
    destinationList.lastChild.selected = false;
  }
};

MoveElements.prototype.bindEvent = function() {
  this.add.addEventListener('click', this.eventListener.bind(this, this.add));
  this.remove.addEventListener('click', this.eventListener.bind(this, this.remove));
};

window.onload = function() {
  var ids1 = {
    add : document.getElementById("add1"),
    remove : document.getElementById("remove1"),
    destinationList : document.getElementById("destination1"),
    sourceList : document.getElementById("source1"),
  }
  var ids2 = {
    add : document.getElementById("add2"),
    remove : document.getElementById("remove2"),
    destinationList : document.getElementById("destination2"),
    sourceList : document.getElementById("source2"),
  }
  var countries = new MoveElements(ids1);
  countries.bindEvent();
  var states = new MoveElements(ids2);
  states.bindEvent();
};
