function Form(inputElements) {
  this.requiredFields = inputElements.requiredFields;
  this.isSubmissionCorrect = true;
  this.inputValidator = new InputValidator();
  this.minimumLengthRequiredFields = inputElements.minimumLengthRequiredFields;
  this.submitButton = inputElements.submitButton;
  this.confirmNotification = inputElements.confirmNotification;
};

Form.prototype.submissionIncorrect = function() {
  this.isSubmissionCorrect = false;
};

Form.prototype.submitForm = function() {
  this.isSubmissionCorrect = true;
  this.checkInvalidInputs();
  this.checkMinimumLength();
  this.confirmNotificationAlert();
  if (!this.isSubmissionCorrect) {
    event.preventDefault();
  }
};

Form.prototype.confirmNotificationAlert = function() {
  var check = true;
  if (this.confirmNotification.checked) {
    check = confirm("Confirm Recieve Notification");
  }
  if (!check) {
    this.confirmNotification.checked = check;
    event.preventDefault();
  }
};

Form.prototype.checkInvalidInputs = function() {
  for (var i = 0, length = this.requiredFields.length; i < length; i++) {
    if (!this.inputValidator.validatePresence(this.requiredFields[i])) {
      this.submissionIncorrect();
    }
  }
};

Form.prototype.checkMinimumLength = function(element) {
  for (var i = 0, length = this.minimumLengthRequiredFields.length; i < length; i++) {
    if (this.inputValidator.isInputLengthInvalid(this.minimumLengthRequiredFields[i])) {
      this.submissionIncorrect();
    }
  }
};

Form.prototype.onSubmitClick = function() {
  var _this = this;
  this.submitButton.addEventListener('click', function() {
    _this.submitForm();
  });
};

window.onload = function() {
  var inputElements = {
    requiredFields: document.getElementsByClassName("required"),
    minimumLengthRequiredFields: document.getElementsByClassName("minimum-length-required"),
    submitButton: document.form.button,
    confirmNotification: document.getElementById("confirm")
  };
  var newForm = new Form(inputElements);
  newForm.onSubmitClick();
};
