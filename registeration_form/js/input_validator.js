function InputValidator() {};

InputValidator.prototype.validatePresence = function(element) {
  var isValid = element.value;
  if (!isValid) {
    alert(`${ element.getAttribute("name") } cannot be empty.`);
  }
  return isValid;
};

InputValidator.prototype.isInputLengthInvalid = function(element) {
  var limit = element.getAttribute("limit");
  var isInvalid = element.value.length < limit;
  if (isInvalid) {
    alert(`${ element.getAttribute("name") } should be more than ${ limit } Characters`);
  }
  return isInvalid;
};

InputValidator.prototype.isCheckboxInputIncorrect = function(element) {
  var isInvalid = !element.checked;
  if (isInvalid) {
    alert(`Please select ${ element.getAttribute("name") }.`);
  }
  return isInvalid;
};
