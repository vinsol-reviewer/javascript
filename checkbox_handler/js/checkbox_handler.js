function CheckBoxHandler(link, value) {
  this.link = link;
  this.value = value;
  this.list = document.getElementsByClassName("colors");
}

CheckBoxHandler.prototype.changeState = function() {
  for (i = 0; i < this.list.length; i++) {
    this.list[i].checked = this.value;
  }
};

CheckBoxHandler.prototype.bindEvents = function() {
  this.link.addEventListener('click', this.changeState.bind(this));
}

window.onload = function() {
  checkLink = new CheckBoxHandler(document.getElementById("check-link"), true);
  uncheckLink = new CheckBoxHandler(document.getElementById("uncheck-link"), false);
  checkLink.bindEvents();
  uncheckLink.bindEvents();
}
