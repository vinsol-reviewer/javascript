function CheckBox(ids) {
  this.selectedList = [];
  this.checkList = ids.list;
  this.none = ids.none;
}

CheckBox.prototype.checked = function(day) {
  this.none.checked = false;
  this.addSelectedDay(day);
};

CheckBox.prototype.addSelectedDay = function(day) {
  if (this.selectedList.length < 3) {
    this.selectedList.push(day.getAttribute("value"));
  } else {
    this.listFull(day);
  }
};

CheckBox.prototype.listFull = function(day) {
  day.checked = false;
  alert('Only 3 days can be selected.You have already selected ' + this.selectedList);
};

CheckBox.prototype.clearList = function() {
  this.selectedList.length = 0;
};

CheckBox.prototype.selectNone = function() {
  for (i = 0; i < this.checkList.length; i++) {
    this.checkList[i].checked = false;
  }
  this.clearList();
};

CheckBox.prototype.setEventListeners = function() {
  for (i = 0; i < this.checkList.length; i++) {
    this.checkList[i].addEventListener('change', this.selectDay.bind(this, this.checkList[i]));
  }
  this.none.addEventListener('change', this.selectNone.bind(this, this.none));
};

CheckBox.prototype.selectDay = function(day) {
  if (day.checked) {
    this.checked(day);
  }
  else {
    this.selectedList.splice(this.selectedList.indexOf(day), 1);
  }
};

window.onload = function() {
  var ids = {
    list : document.getElementsByClassName("day"),
    none : document.getElementById("none")
  };
  var selector = new CheckBox(ids);
  selector.setEventListeners();
}
