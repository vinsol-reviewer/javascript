function PromptName(ids) {
  this.fullName;
  this.element = ids.element;
}

PromptName.prototype.showPrompt = function() {
  var f_name = this.setName("First Name");
  var l_name = this.setName("Last Name");
  this.fullName = `Hello ${ f_name } ${ l_name }`;
  alert(this.fullName);
  this.element.innerHTML = this.fullName;
};

PromptName.prototype.setName = function(name) {
  var _name = "";
  while (!_name.trim()) {
    _name = prompt(name);
  }
  return _name;
};

window.onload = function() {
  var ids = {
    element : document.getElementById("p1")
  };
  var promptname = new PromptName(ids);
  promptname.showPrompt();
};
