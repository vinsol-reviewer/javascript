function InputValidator() {
  this.URL_REGEX = new RegExp(/^(https|http):\/\/\w+\..+/);
  this.EMAIL_REGEX = new RegExp(/^\S+@\w+.\w+$/i);
  this.isInvalid;
}

InputValidator.prototype.isTextInputIncorrect = function(element) {
  this.isInvalid = !element.value;
  if (this.isInvalid) {
    alert(`${ element.getAttribute("name") } cannot be empty.`);
  }
  return this.isInvalid;
};

InputValidator.prototype.isTextAreaInputIncorrect = function(element) {
  var limit = element.getAttribute("limit");
  this.isInvalid = element.value.length < limit;
  if (this.isInvalid) {
    alert(`${ element.getAttribute("name") } should be more than ${limit} Characters`);
  }
  return this.isInvalid;
};

InputValidator.prototype.alertInvalidInput = function(_regex, element) {
  this.isInvalid = !_regex.test(element.value);
  if (this.isInvalid) {
    alert(`${ element.getAttribute("name") } is Invalid.`);
  }
  return this.isInvalid;
};

InputValidator.prototype.isInputInvalid = function(element) {
  if (element.getAttribute("type") == "link")
    return this.alertInvalidInput(this.URL_REGEX, element);
  else if(element.getAttribute("type") == "mail")
    return this.alertInvalidInput(this.EMAIL_REGEX, element);
};
