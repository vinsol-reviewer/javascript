function FormManager(inputElements, confirmNotification) {
  this.inputElements = inputElements;
  this.isSubmissionCorrect = true;
  this.inputValidator = new InputValidator();
  this.confirmNotification = confirmNotification;
}

FormManager.prototype.sumissionIncorrect = function(text) {
  this.isSubmissionCorrect = false;
};

FormManager.prototype.submitForm = function() {
  this.isSubmissionCorrect = true;
  for (i = 0; i < this.inputElements.length; i++) {
    var inputType = this.inputElements[i].getAttribute("type");
    switch(inputType) {

      case "text":
        if (this.inputValidator.isTextInputIncorrect(this.inputElements[i])) {
          this.sumissionIncorrect();
        }
        break;

      case "mail":
        if (this.inputValidator.isInputInvalid(this.inputElements[i])) {
          this.sumissionIncorrect();
        }
        break;

      case "textarea":
        if (this.inputValidator.isTextAreaInputIncorrect(this.inputElements[i])) {
          this.sumissionIncorrect();
        }
        break;

      case "link":
        if (this.inputValidator.isInputInvalid(this.inputElements[i])) {
          this.sumissionIncorrect();
        }
        break;
    }
  }
  this.confirmNotificationAlert();
  if(!this.isSubmissionCorrect){
    event.preventDefault();
  }
};

FormManager.prototype.confirmNotificationAlert = function() {
  var check = true;
  if (this.confirmNotification.checked) {
    check = confirm("Confirm Recieve Notification");
  }
  if(!check) {
    this.confirmNotification.checked = check;
    event.preventDefault();
  }
};

FormManager.prototype.onSubmitClick = function(submit) {
  submit.addEventListener('click', this.submitForm.bind(this));
};

window.onload = function() {
  var inputElements = document.getElementsByClassName("required");
  var confirmNotification = document.getElementById("confirm");
  var newForm = new FormManager(inputElements, confirmNotification);
  newForm.onSubmitClick(document.form.button);
}
