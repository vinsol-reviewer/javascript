function Form(ids) {
  this.number = ids.number;
  this.result = ids.result;
}

Form.prototype.submitForm = function() {
  if (Number(this.number.value) && parseFloat(this.number.value)) {
    this.result.value = true;

  }
  else {
    this.result.value = false;
    event.preventDefault();
  }
};

Form.prototype.bindEvent = function(element) {
  var _this = this;
  element.addEventListener('click', function() {
    _this.submitForm();
  });
};

window.onload = function() {
  var ids = {
    number : document.getElementById("numeric"),
    result : document.getElementById("result")
  }
  var form = new Form(ids);
  form.bindEvent(document.getElementById("submit-form"));
};

getValuesFromUrl = function() {
  var qs = window.location.search.length > 0 ? location.search.substring(1) : "";
  var items = qs.length ? qs.split("&") : [];
  var args = {};
  var item = null;
  var name = null;
  var value = null;
  for (i = 0;i < items.length;i++) {
    item = items[i].split("=");
    name = item[0];
    value = item[1];
    args[name] = value;
  }
  return args;
};

(function() {
  var args = getValuesFromUrl();
  var number = document.getElementById("numeric");
  var result = document.getElementById("result");
  if (args.number && args.result_value) {
    number.value = args.number;
    result.value = args.result_value;
  }
})();
